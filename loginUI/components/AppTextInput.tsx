import React, {useState} from "react";
import Colors from "../constants/Colors";
import Font from "../constants/Font";
import FontSize from "../constants/FontSize";
import Spacing from "../constants/Spacing";
import {TextInput, TextInputProps} from "react-native";

const AppTextInput: React.FC<TextInputProps> = ({...otherProps}) =>{
    const [focused, setFocused] = useState<boolean>(false)
    return (
        <TextInput
            onFocus={() => {
                setFocused(true)}
            }
            onBlur={() => setFocused(false)}
            placeholderTextColor={Colors.darkText}
            style={[
                {
                    fontFamily: Font["poppins-regular"],
                    fontSize: FontSize.small,
                    padding: Spacing * 2,
                    backgroundColor: Colors.lightPrimary,
                    borderRadius: Spacing,
                    marginVertical: Spacing,
                }, focused && {
                    outlineStyle: 'none',
                    borderColor: Colors.primary,
                    borderWidth: 2,
                    shadowOffset: {
                        width: 4,
                        height: Spacing
                    },
                    shadowColors: Colors.primary,
                    shadowOpacity: 0.1,
                    shadowRadius: Spacing
                }
            ]}
            {...otherProps}
        />
    );
}

export default AppTextInput