import { StatusBar } from 'expo-status-bar';
import {StyleSheet, Text, View, Image, ImageBackground, Dimensions, SafeAreaView, TouchableOpacity} from 'react-native';
import FontSize from "./constants/FontSize";
import Colors from "./constants/Colors";
import Font from "./constants/Font";
import Spacing from "./constants/Spacing";
import WelcomeScreen from "./screens/WelcomeScreen";
import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";

const {height} =Dimensions.get("window")

export default function App() {
    return (
        <WelcomeScreen/>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
