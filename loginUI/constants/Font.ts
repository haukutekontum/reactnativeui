const poppinsRegular = "../assets/fonts/Poppins-Regular"
const poppinsBold = "../assets/fonts/Poppins-Bold"
const poppinsSemiBold = "../assets/fonts/Poppins-SemiBold"

export default {
    "poppins-regular": poppinsRegular,
    "poppins-bold": poppinsBold,
    "poppins-semiBold": poppinsSemiBold
}
