import React from "react";
import {Dimensions, ImageBackground, SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import Spacing from "../constants/Spacing";
import FontSize from "../constants/FontSize";
import Colors from "../constants/Colors";
import Font from "../constants/Font";
import {RootStackParamList} from "../types";
import {NativeStackScreenProps} from "@react-navigation/native-stack"

const {height} = Dimensions.get("window")

type Props = NativeStackScreenProps<RootStackParamList, 'Welcome'>;

const WelcomeScreen: React.FC<Props> = ({ navigation }) => {
    return (
        <SafeAreaView style={{
            alignContent: 'center',
            justifyContent: 'center',
        }}>
            <View>
                <ImageBackground
                    source={require("../assets/images/welcome-img.png")}
                    style={{
                        width: 500,
                        height: height / 2.5
                    }}
                    resizeMode={"contain"}/>
            </View>
            <View style={{
                paddingHorizontal: Spacing * 4,
                paddingTop: Spacing * 4
            }}>
                <Text style={{
                    fontSize: FontSize.xxLarge,
                    color: Colors.primary,
                    fontFamily: Font["poppins-regular"],
                    textAlign: "center",
                }}>Discover your dream job here</Text>

                <Text style={{
                    fontSize: FontSize.small,
                    color: Colors.text,
                    fontFamily: Font["poppins-regular"],
                    textAlign: "center",
                    marginTop: Spacing * 2
                }}>Explore all the existing job roles based or your
                    interest and study major</Text>
            </View>
            <View style={{
                paddingHorizontal: Spacing * 2,
                paddingTop: Spacing * 6,
                flexDirection: "row"
            }}>
                <TouchableOpacity
                    onPress={() => navigation.navigate("Login")}
                    style={{
                        backgroundColor: Colors.primary,
                        paddingVertical: Spacing * 1.5,
                        paddingHorizontal: Spacing * 2,
                        width: "48%",
                        borderRadius: Spacing,
                        shadowColor: Colors.primary,
                        shadowOffset: {
                            width: 0,
                            height: Spacing
                        },
                        shadowOpacity: 0.3,
                        shadowRadius: Spacing
                    }}>
                    <Text style={{
                        fontFamily: Font["poppins-bold"],
                        color: Colors.onPrimary,
                        fontSize: FontSize.large,
                        textAlign: "center"
                    }}>Login</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => navigation.navigate("Register")}
                    style={{
                        paddingVertical: Spacing * 1.5,
                        paddingHorizontal: Spacing * 2,
                        width: "48%",
                        borderRadius: Spacing,
                        showColor: Colors.primary,
                    }}>
                    <Text style={{
                        fontFamily: Font["poppins-bold"],
                        color: Colors.text,
                        fontSize: FontSize.large,
                        textAlign: "center"
                    }}>Register</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}

export default WelcomeScreen
