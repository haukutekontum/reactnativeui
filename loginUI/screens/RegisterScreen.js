import React from "react";
import {SafeAreaView, Text, TextInput, TouchableOpacity, View} from "react-native";
import Spacing from "../constants/Spacing";
import FontSize from "../constants/FontSize";
import Colors from "../constants/Colors";
import Font from "../constants/Font";
import spacing from "../constants/Spacing";
import {Ionicons} from "@expo/vector-icons";
import AppTextInput from "../components/AppTextInput";

const RegisterScreen = () =>{
    return(
        <SafeAreaView>
            <View style={{
                padding: Spacing * 2
            }}>
                <View style={{
                    alignItems: "center"
                }}>
                    <Text style={{
                        fontSize: FontSize.xLarge,
                        color: Colors.primary,
                        fontFamily: Font["poppins-bold"],
                        marginVertical: spacing * 3,
                        fontWeight: "bold"
                    }}>
                        Create account</Text>
                    <Text style={{
                        fontFamily: Font["poppins-bold"],
                        fontSize: FontSize.small,
                        maxWidth: "80%",
                        textAlign: "center",
                        fontWeight: "bold"
                    }
                    }>
                        Create an account so you can explore all the existing jobs</Text>
                </View>
                <View style={{
                    marginVertical: Spacing * 3,
                }}>
                    <AppTextInput placeholder={"Email"}/>

                    <AppTextInput placeholder={"Password"}
                        secureTextEntry/>

                    <AppTextInput
                        placeholder={"Confirm Password"}
                        secureTextEntry/>
                </View>
                <TouchableOpacity style={{
                    padding: Spacing * 2,
                    backgroundColor: Colors.primary,
                    marginVertical: Spacing * 3,
                    borderRadius: Spacing,
                    shadowColor: Colors.primary,
                    shadowOffset: {
                        width: 0,
                        height: Spacing
                    },
                    shadowOpacity: 0.3,
                    shadowRadius: Spacing
                }}>
                    <Text style={{
                        fontFamily: Font["poppins-bold"],
                        color: Colors.onPrimary,
                        textAlign: "center",
                        fontSize: FontSize.large
                    }}>Sign up</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={()=>navigation.navigate("Register")}
                    style={{
                        padding: Spacing,
                    }}>
                    <Text style={{
                        fontFamily: Font["poppins-semiBold"],
                        color: Colors.text,
                        textAlign: "center",
                        fontSize: FontSize.small,
                        fontWeight: "semiBold"
                    }}>Already have an account</Text>
                </TouchableOpacity>

                <View style={{
                    marginVertical: Spacing * 3,
                }}>
                    <Text style={{
                        fontFamily: Font["poppins-semiBold"],
                        color: Colors.primary,
                        textAlign: "center",
                        fontSize: FontSize.small,
                        fontWeight: "semiBold"
                    }}>Or continue with</Text>
                    <View style={{
                        marginTop: Spacing,
                        flexDirection: "row",
                        justifyContent: "center"
                    }}>
                        <TouchableOpacity style={{
                            padding: Spacing,
                            backgroundColor: Colors.gray,
                            borderRadius: Spacing / 2,
                            marginHorizontal: Spacing
                        }}>
                            <Ionicons
                                name={"logo-google"}
                                color={Colors.text}
                                size={Spacing * 2}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity style={{
                            padding: Spacing,
                            backgroundColor: Colors.gray,
                            borderRadius: Spacing / 2,
                            marginHorizontal: Spacing
                        }}>
                            <Ionicons
                                name={"logo-apple"}
                                color={Colors.text}
                                size={Spacing * 2}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity style={{
                            padding: Spacing,
                            backgroundColor: Colors.gray,
                            borderRadius: Spacing / 2,
                            marginHorizontal: Spacing
                        }}>
                            <Ionicons
                                name={"logo-facebook"}
                                color={Colors.text}
                                size={Spacing * 2}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default RegisterScreen